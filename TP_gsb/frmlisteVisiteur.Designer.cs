﻿namespace TP_gsb
{
    partial class frmlisteVisiteur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbtitre = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbtitre
            // 
            this.lbtitre.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbtitre.AutoSize = true;
            this.lbtitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtitre.ForeColor = System.Drawing.Color.DimGray;
            this.lbtitre.Location = new System.Drawing.Point(94, 67);
            this.lbtitre.Name = "lbtitre";
            this.lbtitre.Size = new System.Drawing.Size(363, 25);
            this.lbtitre.TabIndex = 2;
            this.lbtitre.Text = "LISTE DES VISITEURS MEDICAUX";
            // 
            // frmlisteVisiteur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 406);
            this.Controls.Add(this.lbtitre);
            this.Name = "frmlisteVisiteur";
            this.Text = "Liste des visiteurs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbtitre;
    }
}