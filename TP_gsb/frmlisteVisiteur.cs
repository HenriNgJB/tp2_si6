﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_gsb.mesClasses;
using TP_gsb.mesClasses.outils;
using MySql.Data.MySqlClient;

namespace TP_gsb
{

    public partial class frmlisteVisiteur : Form
    {
        // Déclaration de la variable objet DataGridView initialisée à null
        DataGridView dglisteVisiteur = null;
        // Déclaration des bouton (créés dynamiquement dans la procédure btnInit())
        Button btnInsertDg = null, btnsuppVisiteur = null, btnupdateVisiteur = null, btnquitter = null;
        DataSet ods = null;
        public frmlisteVisiteur()
        {
            InitializeComponent();

            /*Cvisiteurs ovisiteurs = new Cvisiteurs();
            if(ovisiteurs.ovisiteurAuthentifie.estAuthentifie)
            {
                label1.Text = "Il est authentifié mon cher ISSAM !";
            }
            else
            {
                label1.Text = "Il n'est pas authetifié mon cher ISSAM !";

            }*/

            this.MinimumSize = new System.Drawing.Size(1210, 745);

            // appel de la méthode procédure d'initialisation du dataGrigView
            dginit();
            // appel de la méthode procédure d'initialisation du boutons
            btnInit();
          
            this.lbtitre.Location = new System.Drawing.Point(89 + this.dglisteVisiteur.Width / 2 - this.lbtitre.Width /2, 100);
            
           
        }
        
        private void btnInit()
        {
            btnInsertDg = new Button();
            btnsuppVisiteur = new Button();
            btnupdateVisiteur = new Button();
            btnquitter = new Button();
            // 
            // btnInsertDg
            // 
            this.btnInsertDg.BackColor = System.Drawing.Color.Ivory;
            this.btnInsertDg.Location = new System.Drawing.Point(19, 351);
            this.btnInsertDg.Name = "btnInsertDg";
            this.btnInsertDg.Size = new System.Drawing.Size(149, 41);
            this.btnInsertDg.TabIndex = 1;
            this.btnInsertDg.Text = "Ajouter un visiteur";
            this.btnInsertDg.UseVisualStyleBackColor = false;
            this.btnInsertDg.Click += new System.EventHandler(this.button_click); // "Button_click" : La procédure commune à tous les boutons (Button)
            // 
            // btnsuppVisiteur
            // 
            this.btnsuppVisiteur.BackColor = System.Drawing.Color.Ivory;
            this.btnsuppVisiteur.Location = new System.Drawing.Point(193, 351);
            this.btnsuppVisiteur.Name = "btnsuppVisiteur";
            this.btnsuppVisiteur.Size = new System.Drawing.Size(139, 40);
            this.btnsuppVisiteur.TabIndex = 3;
            this.btnsuppVisiteur.Text = "Supprimer visiteur";
            this.btnsuppVisiteur.UseVisualStyleBackColor = false;
            this.btnsuppVisiteur.Click += new System.EventHandler(this.button_click);
            // 
            // btnupdateVisiteur
            // 
            this.btnupdateVisiteur.BackColor = System.Drawing.Color.Ivory;
            this.btnupdateVisiteur.Location = new System.Drawing.Point(353, 351);
            this.btnupdateVisiteur.Name = "btnupdateVisiteur";
            this.btnupdateVisiteur.Size = new System.Drawing.Size(227, 40);
            this.btnupdateVisiteur.TabIndex = 4;
            this.btnupdateVisiteur.Text = "Mise à jour données visiteur";
            this.btnupdateVisiteur.UseVisualStyleBackColor = false;
            this.btnupdateVisiteur.Click += new System.EventHandler(this.button_click);
            // 
            // btnquitter
            // 
            this.btnquitter.BackColor = System.Drawing.Color.Ivory;
            this.btnquitter.Location = new System.Drawing.Point(612, 351);
            this.btnquitter.Name = "btnquitter";
            this.btnquitter.Size = new System.Drawing.Size(132, 40);
            this.btnquitter.TabIndex = 5;
            this.btnquitter.Text = "Quitter";
            this.btnquitter.UseVisualStyleBackColor = false;
            this.btnquitter.Click += new System.EventHandler(this.button_click);

            this.btnInsertDg.Location = new System.Drawing.Point(89, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);
            this.btnsuppVisiteur.Location = new System.Drawing.Point(89 + btnInsertDg.Width + 10, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);
            this.btnupdateVisiteur.Location = new System.Drawing.Point(89 + btnInsertDg.Width + 10 + btnsuppVisiteur.Width + 10, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);
            this.btnquitter.Location = new System.Drawing.Point(89 + btnInsertDg.Width + 10 + btnsuppVisiteur.Width + 10 + btnupdateVisiteur.Width + 10, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);

            this.Controls.Add(btnsuppVisiteur);
            this.Controls.Add(btnInsertDg);
            this.Controls.Add(btnupdateVisiteur);
            this.Controls.Add(btnquitter);

            //positionnement graphique des boutons relativement au datagrid
            this.btnInsertDg.Location = new System.Drawing.Point(89, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);
            this.btnsuppVisiteur.Location = new System.Drawing.Point(89 + btnInsertDg.Width + 10, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);
            this.btnupdateVisiteur.Location = new System.Drawing.Point(89 + btnInsertDg.Width + 10 + btnsuppVisiteur.Width + 10, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);
            this.btnquitter.Location = new System.Drawing.Point(89 + btnInsertDg.Width + 10 + btnsuppVisiteur.Width + 10 + btnupdateVisiteur.Width + 10, this.dglisteVisiteur.Location.Y + this.dglisteVisiteur.Size.Height + 20);

        } 

        /// <summary>
        /// 
        /// </summary>
        private void dginit()
        {
            this.dglisteVisiteur = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dglisteVisiteur)).BeginInit();
            //this.SuspendLayout();
            // 
            // dglisteVisiteur
            // 
            this.dglisteVisiteur.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dglisteVisiteur.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dglisteVisiteur.Location = new System.Drawing.Point(89, 145);
            this.dglisteVisiteur.Name = "dglisteVisiteur";
            this.dglisteVisiteur.RowTemplate.Height = 24;
            this.dglisteVisiteur.Size = new System.Drawing.Size(960,300);
            this.dglisteVisiteur.TabIndex = 0;

            this.dglisteVisiteur.MaximumSize = new System.Drawing.Size(960, 384);

            this.Controls.Add(this.dglisteVisiteur);

            ((System.ComponentModel.ISupportInitialize)(this.dglisteVisiteur)).EndInit();

            // Liaison evenement cellclick a selectionEnregDg
            this.dglisteVisiteur.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.selectionEnregDg);

            
            rafraichirAffichageDg();
        }

        private void rafraichirAffichageDg()
        {
            Cvisiteurs ovisiteurs = Cvisiteurs.getInstance();
            // appel de la méthode getdsVisiteur de l'objet de contrôle qui renvoie un objet de type DataSet
            ods = ovisiteurs.getdsVisiteur();
            //On pourrait passer par la collection de visiteurs de l'objet de contrôle mais dans ce cas le DG est en lecture seule ce qui n'est pas le but ici
            //Dictionary<string, Cvisiteur> ocoll = ovisiteurs.ocollDicovisit;
            // Affectation de l'objet DataSet comme source de données du dataGridview
            dglisteVisiteur.DataSource = ods.Tables["dgVisiteur"].DefaultView;
            //dglisteVisiteur.DataSource = ocoll.Values.ToList<Cvisiteur>();
            dglisteVisiteur.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
            dglisteVisiteur.Refresh();

        }


        string idaSupprimer = null;
        DataGridViewRow orowDg = null;


        private void selectionEnregDg(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int numRow = e.RowIndex;
                // afin de ne pas faire le traitement quand les en-têtes de colonne sont sélectionnées
                if (numRow != -1)
                {

                    DataGridView odg = (DataGridView)sender;
                    orowDg = odg.Rows[numRow];

                    //pour éviter exception quand on sélectionne le row vide pour saisir un nouveau visiteur
                    if (!(orowDg.Cells[0].Value is System.DBNull))
                    {
                        idaSupprimer = (string)orowDg.Cells[0].Value;
                    }
                }

            }
            catch
            {
                MessageBox.Show("Vous ne pouvez pas selectionner plus d'un enregistrement ! Merci.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        //Procédure de l'ensemble des boutons (sauf quitter)
        private void button_click(object sender, EventArgs e)
        {
            Button obtn = (Button)sender;

            switch(obtn.Name)
            {
                case "btnsuppVisiteur":
                    if (idaSupprimer != null)
                    {
                        DialogResult Dlgresult = MessageBox.Show("Etes-vous sûr de vouloir supprimer l'enregistrement n° " + idaSupprimer + " ?", "Avertissement", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (Dlgresult == DialogResult.Yes)
                        {
                            try
                            {
                                Cdao odao = new Cdao();
                                odao.deleteEnreg("delete from visiteur where id='" + idaSupprimer + "'");
                                rafraichirAffichageDg();
                                idaSupprimer = null;
                                MessageBox.Show("Enregistrement supprimé !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            catch (MySqlException ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Veuillez sélectionner un enregistrement à supprimer en cliquant dans la marge de couleur blanche de l'enregistrement choisi ! Merci.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case "btnupdateVisiteur":
                    if (idaSupprimer != null)
                    {
                        string idaupdate = idaSupprimer;
                        DialogResult Dlgresult = MessageBox.Show("Etes-vous sûr de vouloir mettre à jour l'enregistrement n° " + idaSupprimer + " ?", "Avertissement", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (Dlgresult == DialogResult.Yes)
                        {
                            try
                            {
                                string id = (string)this.orowDg.Cells[0].Value;
                                string nom = (string)this.orowDg.Cells[1].Value;
                                string prenom = (string)this.orowDg.Cells[2].Value;
                                string login = (string)this.orowDg.Cells[3].Value;
                                string mdp = (string)this.orowDg.Cells[4].Value;
                                string adresse = (string)this.orowDg.Cells[5].Value;
                                string cp = (string)this.orowDg.Cells[6].Value;
                                string ville = (string)this.orowDg.Cells[7].Value;
                                DateTime odateEmbauche = Convert.ToDateTime(this.orowDg.Cells[8].Value);
                                string dateEmbaucheMySql = CtraitementDate.getDateFormatMysql(odateEmbauche);

                                //je mets tout à jour c'est plus simple
                                string queryUpdate = "update visiteur " +
                                               "set id='" + id.Trim() + "'," +
                                                    "nom='" + nom.Trim() + "'," +
                                                    "prenom='" + prenom.Trim() + "'," +
                                                    "login='" + login.Trim() + "'," +
                                                    "mdp='" + mdp.Trim() + "'," +
                                                    "adresse='" + adresse.Trim() + "'," +
                                                    "cp='" + cp.Trim() + "'," +
                                                    "ville='" + ville.Trim() + "'," +
                                                    "dateEmbauche='" + dateEmbaucheMySql + "' where id='" + idaupdate + "'";

                                Cdao odao = new Cdao();

                                odao.updateEnreg(queryUpdate);

                                rafraichirAffichageDg();
                                //Remise à null des variables objets
                                idaSupprimer = null;
                                this.orowDg = null;
                                MessageBox.Show("Enregistrement mis à jour !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            catch (MySqlException ex)
                            {
                                MessageBox.Show(ex.Message, "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Veuillez sélectionner un enregistrement à mettre à jour en cliquant dans la marge de couleur blanche de l'enregistrement choisi." + "\r\n" + "Puis modifier les cellules à mettre à jour. Merci.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case "btnInsertDg":
                    //int nbEnregInDs = this.ods.Tables[0].Rows.Count;
                    int nbEnreg = dglisteVisiteur.Rows.Count; // cf rowCount
                    DataGridViewRow orowDg = dglisteVisiteur.Rows[nbEnreg - 2];
                    bool dejaException = false;
                    string queryInsert = null;

                    try
                    {
                        string id = (string)orowDg.Cells[0].Value;
                        string nom = (string)orowDg.Cells[1].Value;
                        string prenom = (string)orowDg.Cells[2].Value;
                        string login = (string)orowDg.Cells[3].Value;
                        string mdp = (string)orowDg.Cells[4].Value;
                        string adresse = (string)orowDg.Cells[5].Value;
                        string cp = (string)orowDg.Cells[6].Value;
                        string ville = (string)orowDg.Cells[7].Value;
                        DateTime odateEmbauche = Convert.ToDateTime(orowDg.Cells[8].Value);
                        string dateEmbaucheMySql = CtraitementDate.getDateFormatMysql(odateEmbauche);

                        queryInsert = "insert into visiteur(id,nom,prenom,login,mdp,adresse,cp,ville,dateEmbauche) values('" + id.Trim() + "','" + nom.Trim() + "','" +
                           prenom.Trim() + "','" + login.Trim() + "','" + mdp.Trim() + "','" + adresse.Trim() + "','" + cp.Trim() + "','" +
                           ville.Trim() + "','" + dateEmbaucheMySql + "')";
                    }
                    catch (Exception ex)
                    {
                        dejaException = true;
                        MessageBox.Show(ex.Message + " Veuillez saisir tous les champs ! Merci.");

                    }


                    if (!dejaException) // Si il n'y a pas eu de première exception
                    {
                        Cdao odao = new Cdao();
                        try //remontée d'exception
                        {
                            odao.insertEnreg(queryInsert);

                            DialogResult result = MessageBox.Show("Enregistrement bien effectué. Merci !");
                            // appel methode rafraichissement du datagrid
                            /* 
                             * demande à la classe de contrôle de fournir un dataset
                             * affecte le dataset comme source de données au datagridView
                             * rafraichit l'affichage du dataGridView
                             * */
                            rafraichirAffichageDg();

                        }
                        catch (MySqlException ex)
                        {
                            MessageBox.Show("Il faut saisir un nouvel enregistrement. Merci !" + "\r\n" + ex.Message, "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }

                    break;
                default:
                    this.Close();
                    break;

            }

        }

    }
}
