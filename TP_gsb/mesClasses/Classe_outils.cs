﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace TP_gsb.mesClasses.outils
{
    public class Cdao
    {
        private string connectionString = "SERVER=127.0.0.1; DATABASE=gsb_frais; UID=root; PASSWORD=";

        public MySqlDataReader getReader(string squery)
        {
            MySqlConnection ocnx = new MySqlConnection(connectionString);
            ocnx.Open();
            MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
            MySqlDataReader ord = ocmd.ExecuteReader();
            return ord;
        }

        public string insertEnreg(string squery)
        {
            try
            {
                MySqlConnection ocnx = new MySqlConnection(connectionString);
                ocnx.Open();
                MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
                int nbEnregAffecte = ocmd.ExecuteNonQuery();
                return null;
            }
            catch (MySqlException e)
            {
                return e.Message;

            }
        }

        public string deleteEnreg(string squery)
        {
            try
            {
                MySqlConnection ocnx = new MySqlConnection(connectionString);
                ocnx.Open();
                MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
                int nbEnregAffecte = ocmd.ExecuteNonQuery();
                return null;
            }
            catch (MySqlException e)
            {
                return e.Message;

            }
        }

        public object recupMaxChampTable(string snomChamp, string snomTable)
        {
            try
            {
                //Solution avec des entiers (max utilise) 
                MySqlConnection ocnx = new MySqlConnection(connectionString);
                ocnx.Open();
                string query = "select max(" + snomChamp + ") from " + snomTable;
                MySqlCommand ocmd = new MySqlCommand(query, ocnx);
                object maxId = ocmd.ExecuteScalar(); //executeScalar renvoie un type object 
                return maxId;
            }
            catch (MySqlException e)
            {
                return (object)e.Message; //executeScalar renvoie un type object

            }

        }

    
   
    }
}