﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TP_gsb.mesClasses.outils;

namespace corr_SI6
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
           
            InitializeComponent();
            tbAffiche.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;


        }



        private void afficherListeVisiteursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string requete = "SELECT id, nom, prenom FROM visiteur";
            Cdao odao = new Cdao();

            MySqlDataReader ord = odao.getReader(requete);

            while (ord.Read())
            {
                tbAffiche.Text = tbAffiche.Text + ord[];
            }

        }
    }
}
